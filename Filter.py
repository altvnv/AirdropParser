class Filter:
    """
    Фильтрует CSV файл по указанному паттерну.
    Фильтр может быть по наличию определенных токенов, участие в определенных ICO,
    по определенному балансу, по определенным операциям
    """
    @staticmethod
    def filter_eth(start, end):
        pass

    @staticmethod
    def filter_token(token, tx, start=0, end=0):
        assert start <= end

    @staticmethod
    def filter_sender(tx, address):
        pass

    @staticmethod
    def filter_receiver(tx, sender):
        pass
