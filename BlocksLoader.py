import datetime
import json
import requests
import csv


class BlocksLoader:

    url = 'https://mainnet.infura.io/ylGWNjPuG0rIrGRJVcLe'
    file = open("transactions.csv", 'w')

    writer = csv.DictWriter(file, fieldnames=['transaction', 'timestamp'])
    writer.writeheader()

    def load_blocks(self, last_timestamp):
        """
        Loads transactions while timestamp greater than chosen
        :param last_timestamp: Timestamp of last block
        """

        current = int(self.get_current_block(), 16)
        curr_timestamp = 0

        while curr_timestamp == 0 or last_timestamp > curr_timestamp:
            block = self.load_block(hex(current))
            curr_timestamp = int(block['timestamp'], 16)
            print(curr_timestamp)
            print('Current block date: ' + self.decode_timestamp(curr_timestamp))
            tx_array = block['transactions']
            for tx in tx_array:
                self.writer.writerow({'transaction': tx, "timestamp": curr_timestamp})
            print(last_timestamp > curr_timestamp)


    def get_current_block(self):
        r = requests.post(self.url,
                          data = self.create_payload(1, 'eth_blockNumber', []))
        return r.json()["result"]

    def load_block(self, hex_number):
        r = requests.post(self.url,
                          data = self.create_payload(1, 'eth_getBlockByNumber', [hex_number, False]))
        return r.json()['result']

    def create_payload(self, id, method, params):
        payload = {
            'jsonrpc':'2.0',
            'id': id,
            'method': method,
            'params': params
        }
        return json.dumps(payload)

    def decode_timestamp(self, timestamp):
        string = datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')
        return string


if __name__ == "__main__":
    bl = BlocksLoader()
    bl.load_blocks(1514768460)
